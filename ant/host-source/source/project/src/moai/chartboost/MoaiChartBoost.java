package com.ziplinegames.moai;

import android.app.Activity;

import com.chartboost.sdk.*;
import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Libraries.CBLogging.Level;

import com.chartboost.sdk.Model.CBError.CBImpressionError;


class MoaiChartBoost {

	private static Activity sActivity = null;

	protected static native void AKUNotifyChartBoostInterstitialDismissed();
	protected static native void AKUNotifyChartBoostInterstitialLoadFailed();
	protected static native void AKUNotifyChartBoostRewardedVideoCompleted();

	private static ChartboostDelegate delegate = new ChartboostDelegate()
	{
		
		@Override
		public boolean shouldDisplayInterstitial(String location) {
			MoaiLog.i ( "MoaiChartboost shouldDisplayInterstitial: " + location );
			return true;
		}
	
		@Override
		public boolean shouldRequestInterstitial(String location) {
			MoaiLog.i ( "MoaiChartboost shouldRequestInterstitial: " + location );
			return true;
		}
	
		@Override
		public void didCacheInterstitial(String location) {
			MoaiLog.i ( "MoaiChartboost didCacheInterstitial: " + location );
		}
	
		@Override
		public void didFailToLoadInterstitial(String location, CBImpressionError error) {
			synchronized ( Moai.sAkuLock ) {
				MoaiLog.i ( "MoaiChartboost didFailToLoadInterstitial: " + location + ", " + error.toString() );
				AKUNotifyChartBoostInterstitialLoadFailed();
			}
		}
	
		@Override
		public void didDismissInterstitial(String location) {
			MoaiLog.i ( "MoaiChartboost didDismissInterstitial: " + location );
			Chartboost.cacheInterstitial(location);
		}
	
		@Override
		public void didCloseInterstitial(String location) {
			synchronized ( Moai.sAkuLock ) {
				MoaiLog.i ( "MoaiChartboost didCloseInterstitial: " + location );
				AKUNotifyChartBoostInterstitialDismissed();
			}
		}
	
		@Override
		public void didClickInterstitial(String location) {
			MoaiLog.i ( "MoaiChartboost didClickInterstitial: " + location );
			// Know that the user has clicked the interstitial
		}
	
		@Override
		public void didDisplayInterstitial(String location) {
			MoaiLog.i ( "MoaiChartboost didShowInterstitial: " + location );
		}
	
		@Override
		public boolean shouldRequestMoreApps(String location) {
			return true;
		}
	
		@Override
		public boolean shouldDisplayMoreApps(String location) {
			return true;
		}
	
		@Override
		public void didFailToLoadMoreApps(String location, CBImpressionError error) {
			// Do something else when the More-Apps page fails to load
		}
	
		@Override
		public void didCacheMoreApps(String location) {
			// Know that the More-Apps page is cached and ready to display
		}
	
		@Override
		public void didDismissMoreApps(String location) {
			// Know that the More-Apps page has been dismissed
		}
	
		@Override
		public void didCloseMoreApps(String location) {
			// Know that the More-Apps page has been closed
		}
	
		@Override
		public void didClickMoreApps(String location) {
			// Know that the More-Apps page has been clicked
	
		}
	
		@Override
		public void didDisplayMoreApps(String location) {
			// Know that the More-Apps page has been presented on the screen
		}
	
		@Override
		public boolean shouldDisplayRewardedVideo(String location) {
			MoaiLog.i(String.format("SHOULD DISPLAY REWARDED VIDEO: '%s'",  (location != null ? location : "null")));
			return true;
		}
	
		@Override
		public void didCacheRewardedVideo(String location) {
			MoaiLog.i(String.format("DID CACHE REWARDED VIDEO: '%s'",  (location != null ? location : "null")));
		}
	
		@Override
		public void didFailToLoadRewardedVideo(String location,	CBImpressionError error) {
			MoaiLog.i(String.format("DID FAIL TO LOAD REWARDED VIDEO: '%s', Error:  %s",  (location != null ? location : "null"), error.name()));
		}
	
		@Override
		public void didDismissRewardedVideo(String location) {
			MoaiLog.i(String.format("DID DISMISS REWARDED VIDEO '%s'",  (location != null ? location : "null")));
		}
	
		@Override
		public void didCloseRewardedVideo(String location) {
			MoaiLog.i(String.format("DID CLOSE REWARDED VIDEO '%s'",  (location != null ? location : "null")));
		}
	
		@Override
		public void didClickRewardedVideo(String location) {
			MoaiLog.i(String.format("DID CLICK REWARDED VIDEO '%s'",  (location != null ? location : "null")));
		}
	
		@Override
		public void didCompleteRewardedVideo(String location, int reward) {
			synchronized ( Moai.sAkuLock ) {
				MoaiLog.i(String.format("DID COMPLETE REWARDED VIDEO '%s' FOR REWARD %d",  (location != null ? location : "null"), reward));
				AKUNotifyChartBoostRewardedVideoCompleted();
			}
		}
		
		@Override
		public void didDisplayRewardedVideo(String location) {
			MoaiLog.i(String.format("DID DISPLAY REWARDED VIDEO '%s' FOR REWARD", location));
		}
		
		@Override
		public void willDisplayVideo(String location) {
			MoaiLog.i(String.format("WILL DISPLAY VIDEO '%s", location));
		}
		
	};


	public static void onCreate ( Activity activity ) {
		MoaiLog.i ( "MoaiChartBoost onCreate: Initializing Chartboost" );
		MoaiLog.i( "MoaiChartBoost onCreate: got activity " + activity.toString() );
		sActivity = activity;
	}

	public static void onDestroy () {
		MoaiLog.i ( "MoaiChartBoost onDestroy: Destroying Chartboost service" );
		Chartboost.onDestroy(sActivity);
	}
	
	//----------------------------------------------------------------//
	public static void onPause ( Activity activity ) {
		MoaiLog.i ( "MoaiChartBoost: onPause" );
    		Chartboost.onPause ( activity );
	}

	//----------------------------------------------------------------//
	public static void onResume ( Activity activity ) {
		MoaiLog.i ( "MoaiChartBoost: onResume" );
    		Chartboost.onResume ( activity );
	}
		
	//----------------------------------------------------------------//
	public static void onStart ( Activity activity ) {
		MoaiLog.i ( "MoaiChartBoost: onStart" );
    		Chartboost.onStart ( activity );
	}
	
	//----------------------------------------------------------------//
	public static void onStop ( Activity activity ) {
		MoaiLog.i ( "MoaiChartBoost: onStop" );
	    	Chartboost.onStop ( activity );
	}

	/* CHECK http://getmoai.com/forums/post15289.html?hilit=chartboost%20android#p15289
	public static void init(String appId, String appSignature) {
		cb = Chartboost.sharedChartboost();
		cb.onCreate(sActivity, appId, appSignature, chartBoostDelegate);
		cb.startSession();
	}
	*/
	
	public static void init(final String appId, final String appSignature) {       
	
		/*
		MoaiLog.i ( "MoaiChartBoost init: " + appId + ", " + appSignature );
	
		Chartboost.startWithAppId(sActivity, appId, appSignature);
		Chartboost.setImpressionsUseActivities(false);
		Chartboost.setLoggingLevel(Level.ALL);
		Chartboost.setDelegate(new MoaiChartBoost());
		Chartboost.onCreate(sActivity);
		Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
		Chartboost.cacheRewardedVideo(CBLocation.LOCATION_DEFAULT);
		*/

		sActivity.runOnUiThread(new Runnable() {
			public void run() {
			
				MoaiLog.i ( "MoaiChartBoost init: " + appId + ", " + appSignature );
			
				try {
					Chartboost.startWithAppId(sActivity, appId, appSignature);
		    		Chartboost.onStart (sActivity);
					Chartboost.setDelegate(delegate);
					Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
					Chartboost.cacheRewardedVideo(CBLocation.LOCATION_DEFAULT);
					
				} catch (Exception e) {
					MoaiLog.i ("MoaiChartBoost init error: " + e.toString());
				}
				// cb = Chartboost.sharedChartboost();
				// cb.onCreate(sActivity, appId, appSignature, chartBoostDelegate);
				// cb.onStart(sActivity);
				// cb.startSession();
				// cb.cacheInterstitial();
				// cb.cacheRewardedVideo();
			}
		});

	}
	
	/* CHECK http://getmoai.com/forums/post15289.html?hilit=chartboost%20android#p15289
	public static void showInterstitial(String location) {
		cb.showInterstitial();
	}
	*/
	
	public static void showInterstitial(String location) {
	
		// Chartboost.showInterstitial(CBLocation.LOCATION_DEFAULT);
	
		sActivity.runOnUiThread(new Runnable() {
			public void run() {
				MoaiLog.i("MoaiChartBoost.showInterstitial:");
				Chartboost.showInterstitial(CBLocation.LOCATION_DEFAULT);
			}
		});

	}

	public static void showRewardedVideo(String location) {
	
		// MoaiLog.i ( "MoaiChartBoost showRewardedVideo: " + location );
		// Chartboost.showRewardedVideo(CBLocation.LOCATION_DEFAULT);
	
		sActivity.runOnUiThread(new Runnable() {
			public void run() {
				MoaiLog.i("MoaiChartBoost.showRewardedVideo:");
				Chartboost.showRewardedVideo(CBLocation.LOCATION_DEFAULT);
			}
		});
	}

	public static boolean hasCachedInterstitial() {
		return Chartboost.hasInterstitial(CBLocation.LOCATION_DEFAULT);
	}

	public static boolean hasCachedRewardedVideo() {
		MoaiLog.i ( "MoaiChartBoost hasCachedRewardedVideo? " );
		if (!Chartboost.hasRewardedVideo(CBLocation.LOCATION_DEFAULT)) {
			MoaiLog.i ( "MoaiChartBoost hasCachedRewardedVideo FUCK NO! " );
		}
		
		return Chartboost.hasRewardedVideo(CBLocation.LOCATION_DEFAULT);
	}
	

	/* CHECK http://getmoai.com/forums/post15289.html?hilit=chartboost%20android#p15289
	public static void loadInterstitial(String location) {
		cb.cacheInterstitial();
	}
	*/
	
	public static void loadInterstitial(String location) {
	
		/*
		Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
		Chartboost.cacheRewardedVideo(CBLocation.LOCATION_DEFAULT);
		MoaiLog.i ( "MoaiChartBoost loadInterstitial end" );
		*/

		sActivity.runOnUiThread(new Runnable() {
			public void run() {
				MoaiLog.i ( "MoaiChartBoost loadInterstitial start" );
				Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
				Chartboost.cacheRewardedVideo(CBLocation.LOCATION_DEFAULT);
				MoaiLog.i ( "MoaiChartBoost loadInterstitial end" );
			}
		});
	}
	
	
}
